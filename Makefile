all: tracetuner

hello: hello.c
	gcc -o hello hello.c

tracetuner:
	cd src && make
	mv rel tracetuner
	-mv tracetuner/Linux tracetuner/bin
	-mv tracetuner/Linux_64 tracetuner/bin
	tar czf tracetuner-bin-build${BITBUCKET_BUILD_NUMBER}.tgz tracetuner

clean:
	rm -rf tracetuner
	cd src && make clean
